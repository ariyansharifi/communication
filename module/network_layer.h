#ifndef __NETWORK_LAYER_H__
#define __NETWORK_LAYER_H__

#include "communication_layer.h"
#include "phy_layer.h"


class network_layer{
public:
    static void init(uint32_t _ID = NULL_ID);
    static bool auto_init_ID();
    static bool handle_RX_packet(uint8_t* _data,uint8_t& n,unique_id_t& _from_this_id);
    static void handle_TX_packet(const uint8_t* _data,uint8_t n,uint32_t _to_this_dest_id,packet_policy_t _policy);
private:
    struct ID_and_seq{
        unique_id_t _id;
        uint16_t seq_counter;
        ID_and_seq(){
            _id.ID = NULL_ID;
            seq_counter = 0xFFF0;
        }
    };

    static void send_msg();
    static void calc_direction_broadcast_and_send_packet();
    static bool is_redundant();
    static void help_your_neighbor_to_init_their_IP(module_port_t);
};


#endif
