//
// Created by Ariyan on 9/1/2019.
//

#ifndef SERVER_PHY_LAYER_HPP
#define SERVER_PHY_LAYER_HPP

#include <cstdint>
#include <memory>
#include "fw_common.hpp"


namespace CPU{

    class phy_layer {
    public:
        typedef std::shared_ptr<phy_layer> sptr;
        static sptr make(std::string com_port,double Timeout = 50e3);
        virtual bool read_one_byte(uint8_t& data_i) = 0;
        virtual void write_one_byte(uint8_t data_o) = 0;
    };


}




#endif //SERVER_PHY_LAYER_HPP
