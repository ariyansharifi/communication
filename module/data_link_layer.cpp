#include "data_link_layer.h"
#include "phy_layer.h"
#include "global.h"

#include "Arduino.h"


bool data_link::read_one_byte_with_timeOut(uint8_t& byte,module_port_t _which_port){
    if(phy_layer::read_one_byte(byte,_which_port))
      return true;

#ifdef _ARDUINO_
   delayMicroseconds(READ_TIME_OUT);
#endif
    return phy_layer::read_one_byte(byte,_which_port);
}
bool data_link::handle_incomming_packet(module_port_t _which_port){
  uint8_t state = GET_FIRST_PREAMBLE;
  uint8_t data_i;
  uint8_t packet_size;
  uint8_t * _rx_payload = reinterpret_cast<uint8_t *>(&rxtx_packet);
  while(read_one_byte_with_timeOut(data_i,_which_port)){
    switch (state) {
      case GET_FIRST_PREAMBLE:
          if(data_i == PREAMBLE_1)
            state = GET_SECOND_PREAMBLE;
          else
            return false;
      break;
      case GET_SECOND_PREAMBLE:
        if(data_i == PREAMBLE_2)
          state = GET_SIZE_OF_PACKET;
        else
          return false;
       break;
      case GET_SIZE_OF_PACKET:
        packet_size = data_i;
        rxtx_packet.data_link_header.packet_size=data_i;
        state = DUMMY_CYCLE_FOR_RESERVE;
       break;
      case DUMMY_CYCLE_FOR_RESERVE:
        _rx_payload += sizeof(data_link_header_t);
        state = GET_REST_OF_PACKET;
      break;
      case GET_REST_OF_PACKET:
        *(_rx_payload++)=data_i;
        if(--packet_size == 0)
          return true;
      break;
    }
  }
  return false;
}


bool data_link::recv_packet(module_port_t& _which_port){
  for(uint8_t i=0;i<4;i++){
    if(handle_incomming_packet(static_cast<module_port_t>(i)) ){
        _which_port = static_cast<module_port_t>(i);
        return true;
    }
  }
   return false;
}

bool data_link::send_packet(module_port_t _which_port){
    uint8_t* _tx_byte = reinterpret_cast<uint8_t*>(&rxtx_packet);
    for(uint8_t i=0; i<(sizeof(data_link_header_t) + rxtx_packet.data_link_header.packet_size);i++)
      phy_layer::write_one_byte(*(_tx_byte+i),_which_port);
}

void data_link::init(){
    phy_layer::init();
}
