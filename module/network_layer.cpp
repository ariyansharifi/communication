#include "network_layer.h"
#include "global.h"
#include "data_link_layer.h"
#include "communication_layer.h"
#include "fw_common.h"
#include "Arduino.h"


uint16_t seq_counter;

void network_layer::send_msg(){
    switch (rxtx_packet.network_header.packet_policy){
        case packet_policy_t::deliver_command_via_broadcast:
                calc_direction_broadcast_and_send_packet();
            break;
        default:{} break;
    }
}


void network_layer::help_your_neighbor_to_init_their_IP(module_port_t _which_port){
    if(my_id.ID == NULL_ID)            // I cant help him
        return;
    rxtx_packet.network_header._dest_id.ID=Broadcast_id;
    rxtx_packet.network_header._source_id.ID = my_id.ID;
    rxtx_packet.network_header.auto_negotiation_cmd = auto_negotiation_cmd_t::auto_neg_ID_ack;
    rxtx_packet.network_header.packet_policy = do_not_broadcast_this_msg;
    rxtx_packet.network_header.seq_counter = seq_counter++;
    data_link::send_packet(_which_port);
}

bool network_layer::auto_init_ID(){
    if(my_id.ID != NULL_ID)
        return true;
    rxtx_packet.network_header._dest_id.ID=Broadcast_id;
    rxtx_packet.network_header._source_id.ID = my_id.ID;
    rxtx_packet.network_header.auto_negotiation_cmd = auto_negotiation_cmd_t::auto_neg_ID_request;
    rxtx_packet.network_header.packet_policy = do_not_broadcast_this_msg;
    for(uint8_t i = 0;i<4;i++){
        data_link::send_packet(static_cast<module_port_t >(i));
#ifdef _ARDUINO_
        //delay(100);
#endif
        module_port_t _port = UNKNOWN;
        if(data_link::recv_packet(_port) && (_port == static_cast<module_port_t >(i)) &&
            (rxtx_packet.network_header.auto_negotiation_cmd == auto_negotiation_cmd_t::auto_neg_ID_ack)){
            switch (_port){
                case module_port_t::Right:
                    my_id.dimension.X = rxtx_packet.network_header._source_id.dimension.X -1;
                    my_id.dimension.Y = rxtx_packet.network_header._source_id.dimension.Y;
                    break;
                case module_port_t::Up:
                    my_id.dimension.X = rxtx_packet.network_header._source_id.dimension.X;
                    my_id.dimension.Y = rxtx_packet.network_header._source_id.dimension.Y - 1;
                    break;
                case module_port_t::Down:
                    my_id.dimension.X = rxtx_packet.network_header._source_id.dimension.X;
                    my_id.dimension.Y = rxtx_packet.network_header._source_id.dimension.Y + 1;
                    break;
                case module_port_t::Left:
                    my_id.dimension.X = rxtx_packet.network_header._source_id.dimension.X + 1;
                    my_id.dimension.Y = rxtx_packet.network_header._source_id.dimension.Y;
                    break;
            }
            return true;
        }
    }
    return false;

}


 void network_layer::calc_direction_broadcast_and_send_packet(){
    if(rxtx_packet.network_header._dest_id.ID == Broadcast_id){
      data_link::send_packet(module_port_t::Left);
      data_link::send_packet(module_port_t::Right);
      data_link::send_packet(module_port_t::Down);
      data_link::send_packet(module_port_t::Up);
    }else{

    if(rxtx_packet.network_header._dest_id.dimension.X < my_id.dimension.X)
        data_link::send_packet(module_port_t::Left);
    else if(rxtx_packet.network_header._dest_id.dimension.X > my_id.dimension.X)
        data_link::send_packet(module_port_t::Right);

    if(rxtx_packet.network_header._dest_id.dimension.Y < my_id.dimension.Y)
        data_link::send_packet(module_port_t::Down);
    else if(rxtx_packet.network_header._dest_id.dimension.Y > my_id.dimension.Y)
        data_link::send_packet(module_port_t::Up);

    }
}

 bool network_layer::is_redundant(){
    static uint8_t table_last_index = 0;
    static ID_and_seq prev_packet [3];
    for(uint8_t i=0;i<3;i++)
        if((prev_packet[i].seq_counter==rxtx_packet.network_header.seq_counter) && (prev_packet->_id.ID == rxtx_packet.network_header._source_id.ID))
            return true;
    prev_packet[table_last_index++]._id.ID = rxtx_packet.network_header._source_id.ID;
    prev_packet[table_last_index].seq_counter = rxtx_packet.network_header.seq_counter;
    if(table_last_index == 3)
        table_last_index=0;
    return false;
}

bool network_layer::handle_RX_packet(uint8_t* data_i,uint8_t &n,unique_id_t& packet_recved_from_this_id){
  module_port_t _which_port;
  if(data_link::recv_packet(_which_port)){
      if(is_redundant())
          return false;
      packet_recved_from_this_id = rxtx_packet.network_header._source_id;
      data_i = rxtx_packet.raw_data;
      n = rxtx_packet.data_link_header.packet_size-sizeof(network_header_t);
      if( rxtx_packet.network_header._dest_id.ID == my_id.ID) {
          return true;
      }else if(rxtx_packet.network_header._dest_id.ID == Broadcast_id){
          if(rxtx_packet.network_header.auto_negotiation_cmd == auto_negotiation_cmd_t::normal_packet){
              send_msg();                    //resending msg;
              return true;
          }else if(rxtx_packet.network_header.auto_negotiation_cmd == auto_negotiation_cmd_t::auto_neg_ID_request ) {
              help_your_neighbor_to_init_their_IP(_which_port);
              return false;
          }
          }
      else {
          send_msg();                    //resending msg;
          return false;
      }
  }
}


void network_layer::handle_TX_packet(const uint8_t * data_o,uint8_t n,uint32_t _to_this_id,packet_policy_t _policy){
    rxtx_packet.network_header._dest_id.ID=_to_this_id;
    rxtx_packet.network_header.packet_policy=_policy;
    rxtx_packet.network_header._source_id.ID=my_id.ID;
    rxtx_packet.network_header.seq_counter= seq_counter++;;
    rxtx_packet.data_link_header.packet_size = n + sizeof(network_header_t);
    for(uint8_t i =0;i<n;i++)
      rxtx_packet.raw_data[i]=data_o[i];
    send_msg();
}

void network_layer::init(uint32_t _ID){
    seq_counter  = analogRead(A0)*analogRead(A0);
    my_id.ID=_ID;
    data_link::init();
}
