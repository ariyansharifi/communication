#include "phy_layer.h"

#include "global.h"


/**
  This function should read one byte from the dedicated module port
  return true if the data was available
  return false if the data wasnt available
**/

bool phy_layer::read_one_byte(uint8_t& _data_in,module_port_t _from_this_port){
  switch (_from_this_port) {
    case module_port_t::Right :
    Rightport.listen();
    if(Rightport.available() > 0){
      _data_in = Rightport.read();   //  this line will be changed
        return true;
    }
    break;
    case module_port_t::Up :
      Upport.listen();
      if(Upport.available() > 0){
        _data_in = Leftport.read();   //  this line will be changed
        return true;
      }
    break;
    case module_port_t::Left :
      Leftport.listen();
      if(Leftport.available() > 0){
        _data_in = Leftport.read();   //  this line will be changed
        return true;
      }
           _data_in = Leftport.read();   //  this line will be changed
    break;
    case module_port_t::Down :
    Downport.listen();
    if(Downport.available() > 0){
      _data_in = Downport.read();   //  this line will be changed
      return true;
    }
    break;
  }
  return false;
}


/**
  This function should write one byte to the dedicated module port
**/
void phy_layer::write_one_byte(uint8_t _data_out,module_port_t _from_this_port){
  switch (_from_this_port) {
    case module_port_t::Right :
    Rightport.print(_data_out);
    break;
    case module_port_t::Up :
    Upport.print(_data_out);
    break;
    case module_port_t::Left :
    Leftport.print(_data_out);
    break;
    case module_port_t::Down :
    Rightport.print(_data_out);
    break;
  }

}

void phy_layer::init(){
  // Start each software serial port
  Upport.begin(BaudRate);
  Downport.begin(BaudRate);
  Leftport.begin(BaudRate);
  Rightport.begin(BaudRate);
}
