#ifndef __DATA_LINK_LAYER_H__
#define __DATA_LINK_LAYER_H__

#include "communication_layer.h"
#include "phy_layer.h"

#define READ_TIME_OUT 1e6/BaudRate //Micro seconds


class data_link{
private:
    typedef enum {
        GET_FIRST_PREAMBLE,
        GET_SECOND_PREAMBLE,
        GET_SIZE_OF_PACKET,
        DUMMY_CYCLE_FOR_RESERVE,
        GET_REST_OF_PACKET
    } data_link_recv_state;

    static bool read_one_byte_with_timeOut(uint8_t& byte,module_port_t _which_port);
    static bool handle_incomming_packet(module_port_t _which_port);
public:
    static bool recv_packet(module_port_t& _which_port);
    static bool send_packet(module_port_t _which_port);
    static void init();

};



#endif
