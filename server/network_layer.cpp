//
// Created by Ariyan on 9/1/2019.
//

#include "network_layer.hpp"





CPU::unique_id_t::unique_id_t(){
    ID = NULL_ID;
}
CPU::unique_id_t::unique_id_t(int16_t _X,int16_t _Y){
    dimension.X = _X;
    dimension.Y = _Y;
}
CPU::unique_id_t::unique_id_t(uint32_t _ID){
    ID = _ID;
}

CPU::network_layer::ID_and_seq::ID_and_seq() {
        _id.ID = NULL_ID;
        seq_counter = 0xFFF0;
}


CPU::network_layer::sptr CPU::network_layer::make() {
    return CPU::network_layer::sptr(new network_layer());
}

bool CPU::network_layer::is_redundant(const network_header_t& input_pkt_header) {
    static ID_and_seq prev_packet [256];
    static uint8_t write_index=0;
    for(uint16_t i=0;i<256;i++){
        if(prev_packet[i].seq_counter==input_pkt_header.seq_counter && prev_packet[i]._id.ID == input_pkt_header._source_id.ID)
            return true;
    }
    prev_packet[write_index]._id=input_pkt_header._source_id.ID;
    prev_packet[write_index]._id=input_pkt_header.seq_counter;
    write_index++;
    return false;
}

CPU::network_layer::network_layer(){
    cpu_id.ID = Server_id;
    seq_counter = 0;
    _data_link = data_link::make();
}
void CPU::network_layer::send_pkt(const std::vector<uint8_t> &data_o, CPU::unique_id_t _dest,CPU::packet_policy_t _policy) {
    static network_header_t network_overhead;
    network_overhead._source_id=cpu_id;
    network_overhead._dest_id= _dest;
    network_overhead.seq_counter=seq_counter++;
    network_overhead.packet_policy=_policy;
    network_overhead.auto_negotiation_cmd=auto_negotiation_cmd_t::normal_packet;
    auto ptr = reinterpret_cast<uint8_t *>(&network_overhead);
    auto pkt = std::vector<uint8_t >(ptr, ptr + sizeof(network_overhead));
    pkt.insert(pkt.end(),data_o.begin(),data_o.end());
    _data_link->send_raw_data(pkt);
}

bool CPU::network_layer::rcv_pkt(std::vector<uint8_t> &data_i, CPU::unique_id_t &_src) {
    static network_header_t network_overhead;
    std::vector<uint8_t> temp;
    if(_data_link->receive_raw_data(temp)){
      memcpy(temp.data(), &network_overhead, sizeof(network_header_t));
      if(is_redundant(network_overhead))
        return false;

      if(network_overhead._dest_id.ID == Server_id){
        data_i.resize(temp.size()- sizeof(network_header_t));
        memcpy(temp.data()+ sizeof(network_header_t),data_i.data(),temp.size()- sizeof(network_header_t));
        _src = network_overhead._source_id;
        return true;
      }
      else if(network_overhead._dest_id.ID == Broadcast_id){
        if(network_overhead.auto_negotiation_cmd == auto_negotiation_cmd_t::normal_packet)
          return true;
      }
    }
    return false;
}

CPU::unique_id_t CPU::network_layer::get_id(){return cpu_id;}
void CPU::network_layer::set_id(CPU::unique_id_t _id){cpu_id = _id; }
