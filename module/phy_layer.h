#ifndef __ARDOUINO_PHY_LAYER__
#define __ARDOUINO_PHY_LAYER__

#include "fw_common.h"


/*
            Up
        ----------
        |        |
Left    | Module |   Right
        |        |
        ----------
          Down
*/

enum module_port_t{
  Right = 0,
  Up = 1,
  Left = 2,
  Down = 3,
  UNKNOWN = 4
};

#define BaudRate 9600     // change this if needed

class phy_layer{
public:
    static void init();
    static bool read_one_byte(uint8_t& _data_in,module_port_t _from_this_port);
    static void write_one_byte(uint8_t _data_out,module_port_t _from_this_port);
};

#endif
