#ifndef __COMMUNICATION_LAYER__
#define __COMMUNICATION_LAYER__


#include "fw_common.h"


typedef enum{
  deliver_command_via_broadcast = 'b',
  do_not_broadcast_this_msg = 'd'
}packet_policy_t;

typedef enum{
    normal_packet = 'n',
    auto_neg_ID_request = 'r',
    auto_neg_ID_ack = 'a'
}auto_negotiation_cmd_t;


struct network_header_t{
    unique_id_t _source_id;
    unique_id_t _dest_id;
    uint16_t seq_counter;
    uint8_t packet_policy;
    uint8_t auto_negotiation_cmd;
};


#define PREAMBLE_1 0xAA
#define PREAMBLE_2 0xBB


struct data_link_header_t{
  uint8_t preamble_1 = PREAMBLE_1;
  uint8_t preamble_2 = PREAMBLE_2;
  uint8_t packet_size = sizeof(application_t)+ sizeof(network_header_t);
  uint8_t reserved = 0;
};



struct packet_t{
    packet_t(){;}
    data_link_header_t data_link_header;
    network_header_t network_header;
    uint8_t raw_data [50];
};


#endif
