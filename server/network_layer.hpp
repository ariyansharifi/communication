//
// Created by Ariyan on 9/1/2019.
//

#ifndef SERVER_NETWORK_LAYER_HPP
#define SERVER_NETWORK_LAYER_HPP

#include "data_link.hpp"
#include <memory>
#include "fw_common.hpp"

namespace CPU{


union unique_id_t{
    struct {
        int16_t X;
        int16_t Y;
    }dimension;
    uint32_t ID;
    unique_id_t(int16_t _Y,int16_t _X);
    unique_id_t(uint32_t _ID);
    unique_id_t();
};


typedef enum{
    deliver_command_via_broadcast = 'b',
    do_not_broadcast_this_msg = 'd'
}packet_policy_t;



typedef enum{
    normal_packet = 'n',
    auto_neg_ID_request = 'r',
    auto_neg_ID_ack = 'a'
}auto_negotiation_cmd_t;



    class network_layer {
public:
    typedef std::shared_ptr<CPU::network_layer> sptr;
    static sptr make();
    bool rcv_pkt(std::vector<uint8_t >& data_i,unique_id_t& _src);
    void send_pkt(const std::vector<uint8_t>& data_o,unique_id_t _src,packet_policy_t _policy=deliver_command_via_broadcast);

    CPU::unique_id_t get_id();
    void set_id(CPU::unique_id_t _id);
private:
    data_link::sptr _data_link;
    unique_id_t cpu_id;
    uint16_t seq_counter;
    network_layer();




    struct network_header_t{
        unique_id_t _source_id;
        unique_id_t _dest_id;
        uint16_t seq_counter;
        uint8_t packet_policy;
        uint8_t auto_negotiation_cmd;
    };

    
    struct ID_and_seq{
        unique_id_t _id;
        uint16_t seq_counter;
        ID_and_seq();
    };


    bool is_redundant(const network_header_t& input_pkt_header);

    };

}


#endif //SERVER_NETWORK_LAYER_HPP
