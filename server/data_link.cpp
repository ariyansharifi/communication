//
// Created by Ariyan on 9/1/2019.
//

#include "data_link.hpp"
#include "phy_layer_windows_impl.hpp"
#include <vector>

CPU::data_link::sptr CPU::data_link::make(){
    return CPU::data_link::sptr(new data_link());
}

CPU::data_link::data_link(){
    _phy = CPU::phy_layer::make(std::string("COM1"),Time_out);
}

bool CPU::data_link::receive_raw_data(std::vector<uint8_t> &_rx_payload) {
    uint8_t state = GET_FIRST_PREAMBLE;
    uint8_t data_i;
    uint8_t packet_size;
    while(_phy->read_one_byte(data_i)){
        switch (state) {
            case GET_FIRST_PREAMBLE:
                if(data_i == PREAMBLE_1)
                    state = GET_SECOND_PREAMBLE;
                else
                    return false;
                break;
            case GET_SECOND_PREAMBLE:
                if(data_i == PREAMBLE_2)
                    state = GET_SIZE_OF_PACKET;
                else
                    return false;
                break;
            case GET_SIZE_OF_PACKET:
                packet_size = data_i;
                _rx_payload.clear();
                state = DUMMY_CYCLE_FOR_RESERVE;
                break;
            case DUMMY_CYCLE_FOR_RESERVE:
                state = GET_REST_OF_PACKET;
                break;
            case GET_REST_OF_PACKET:
                _rx_payload.push_back(data_i);
                if(--packet_size == 0)
                    return true;
                break;
        }
    }
    return false;

}

void CPU::data_link::send_raw_data(const std::vector<uint8_t> &data_o) {
    static data_link_header_t _mac_over_head;
    _mac_over_head.packet_size = data_o.size();
    auto ptr = reinterpret_cast<uint8_t *>(&_mac_over_head);
    auto over_head = std::vector<uint8_t >(ptr, ptr + sizeof(_mac_over_head));

    for(uint8_t i=0;i<over_head.size();i++)
        _phy->write_one_byte(over_head[i]);

    for(uint8_t i=0;i<data_o.size();i++)
        _phy->write_one_byte(data_o[i]);

}