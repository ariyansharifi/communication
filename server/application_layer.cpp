//
// Created by Ariyan on 9/2/2019.
//

#include "application_layer.hpp"



CPU::application_layer::application_layer(){
    _network = CPU::network_layer::make();
}

void CPU::application_layer::send_pkt_struct(const CPU::application_t& pkt,CPU::unique_id_t _to_this_module, CPU::packet_policy_t _policy) {
    std::vector<uint8_t> temp(sizeof(pkt));
    memcpy(temp.data(),&pkt, sizeof(pkt));
    _network->send_pkt(temp,_to_this_module,_policy);
}

bool CPU::application_layer::rcv_pkt_struct(CPU::application_t &pkt,unique_id_t _from_this_id){
    std::vector<uint8_t> temp;
    if(!_network->rcv_pkt(temp,_from_this_id))
        return  false;
    memcpy(&pkt,temp.data(), sizeof(pkt));
    return false;
}

void CPU::application_layer::send_move_the_box(CPU::direction_t _dir, CPU::unique_id_t _to_this_module){
    application_t pkt;
    pkt.ctrl_cmd = CPU::ctrl_cmd_t::CTRL_CMD_TRANSACT_ME_SOME_BOX_BRO;
    pkt.info.move_object.direction = _dir;
    pkt.info.move_object.direction = _dir;
    pkt.info.move_object.time_stamp = 10;  //I dont know what is this this is an example
    send_pkt_struct(pkt,_to_this_module);
}

void CPU::application_layer::init_0_0_module_ID(){
    application_t pkt;
    pkt.ctrl_cmd = CPU::ctrl_cmd_t::CTRL_CMD_HERE_IS_YOUR_IP_BRO;
    pkt.info.ID_to_set = CPU::unique_id_t(0,0).ID;
    send_pkt_struct(pkt,unique_id_t(Broadcast_id),CPU::packet_policy_t::do_not_broadcast_this_msg);
}


void CPU::application_layer::handle_rx_pkt() {
    application_t pkt;
    unique_id_t _from_this_module;
    if(rcv_pkt_struct(pkt,_from_this_module)){
        switch(pkt.ctrl_cmd){
            case CTRL_CMD_I_AM_ALIVE_DUDE:
                //the module _from_this_module shows the ID
                break;
            case CTRL_CMD_OMG_TRANSACTED_BOX_DUDE:
                //the module _from_this_module shows the ID
                break;
        }
    }
}

CPU::application_layer::sptr CPU::application_layer::make(){
    return CPU::application_layer::sptr(new CPU::application_layer());
}

