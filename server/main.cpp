#include <iostream>


#include "application_layer.hpp"




int main() {
    CPU::application_layer::sptr _appliation_laye = CPU::application_layer::make();

    //init first module ID to 0 0 ;
    _appliation_laye->init_0_0_module_ID();

    //move box with X= 10 and Y =12 to left;
    _appliation_laye->send_move_the_box(CPU::direction_t::move_it_down,CPU::unique_id_t(10,12));

    //handle RX packet;
    _appliation_laye->handle_rx_pkt();


    return 0;
}