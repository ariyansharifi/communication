//
// Created by Ariyan on 9/1/2019.
//

#include "phy_layer_windows_impl.hpp"
#include <string>
#include <exception>


CPU::phy_layer_windows_impl::phy_layer_windows_impl(std::string port_com, double Timeout){
std::string temp("\\\\.\\");
temp += port_com;
hComm = CreateFile(port_com.c_str(),                //port name
GENERIC_READ | GENERIC_WRITE, //Read/Write
0,                            // No Sharing
NULL,                         // No Security
OPEN_EXISTING,// Open existing port only
0,            // Non Overlapped I/O
NULL);        // Null for Comm Devices

    COMMTIMEOUTS timeouts = { 0 };
    timeouts.ReadIntervalTimeout         = Timeout*10e3; // in milliseconds
    timeouts.ReadTotalTimeoutConstant    = Timeout*10e3; // in milliseconds
    timeouts.ReadTotalTimeoutMultiplier  = Timeout*10e3/5; // in milliseconds
    timeouts.WriteTotalTimeoutConstant   = 50; // in milliseconds
    timeouts.WriteTotalTimeoutMultiplier = 10; // in milliseconds

    SetCommTimeouts(hComm,&timeouts);


    DCB dcbSerialParams = { 0 }; // Initializing DCB structure
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
    GetCommState(hComm, &dcbSerialParams);
    dcbSerialParams.BaudRate = BAUD_RATE;  // Setting BaudRate = 9600
    dcbSerialParams.ByteSize = 8;         // Setting ByteSize = 8
    dcbSerialParams.StopBits = ONESTOPBIT;// Setting StopBits = 1
    dcbSerialParams.Parity   = NOPARITY;  // Setting Parity = None
    SetCommState(hComm, &dcbSerialParams);




    if (hComm == INVALID_HANDLE_VALUE)
        throw std::runtime_error("Serial dosn't exist or it useing by another app");

 }
CPU::phy_layer_windows_impl::~phy_layer_windows_impl() {
    CloseHandle(hComm);//Closing the Serial Port
}
void CPU::phy_layer_windows_impl::write_one_byte(uint8_t data_o) {
    uint8_t lpBuffer[1];lpBuffer[0]=data_o;
    bool status = WriteFile(hComm,        // Handle to the Serial port
                   lpBuffer,     // Data to be written to the port
                   1,  //No of bytes to write
                   &dNoOfBytesWritten, //Bytes written
                   NULL);
    if((!status) || dNoOfBytesWritten != 1)
        throw std::runtime_error("writing data failed");
}

bool CPU::phy_layer_windows_impl::read_one_byte(uint8_t &data_i) {

    char TempChar; //Temporary character used for reading
    DWORD NoBytesRead;

    ReadFile( hComm,           //Handle of the Serial port
              &TempChar,       //Temporary character
              sizeof(TempChar),//Size of TempChar
              &NoBytesRead,    //Number of bytes read
              NULL);

    return (NoBytesRead==1);
}

CPU::phy_layer::sptr CPU::phy_layer::make(std::string com_port, double Timeout) {
    return CPU::phy_layer::sptr(new phy_layer_windows_impl(com_port,Timeout));
}
