//
// Created by Ariyan on 9/1/2019.
//

#ifndef SERVER_DATA_LINK_HPP
#define SERVER_DATA_LINK_HPP

#include <vector>
#include "phy_layer.hpp"
#include <memory>


namespace CPU{



class data_link {
public:
    typedef std::shared_ptr<data_link> sptr;
    static sptr make();
    void send_raw_data(const std::vector<uint8_t> &data_o);
    bool receive_raw_data(std::vector<uint8_t>& data_o);

private:
    data_link();
    CPU::phy_layer::sptr _phy;
    double Time_out = 50e-3;

    typedef enum {
        GET_FIRST_PREAMBLE,
        GET_SECOND_PREAMBLE,
        GET_SIZE_OF_PACKET,
        DUMMY_CYCLE_FOR_RESERVE,
        GET_REST_OF_PACKET
    } data_link_recv_state;

    static const uint8_t PREAMBLE_1 = 0xAA;
    static const uint8_t PREAMBLE_2 = 0xBB;


    struct data_link_header_t{
        uint8_t preamble_1;
        uint8_t preamble_2;
        uint8_t packet_size;
        uint8_t reserved;

        data_link_header_t(){
            preamble_1 = data_link::PREAMBLE_1;
            preamble_1 = data_link::PREAMBLE_2;
        }
    };



    };



}



#endif //SERVER_DATA_LINK_HPP
