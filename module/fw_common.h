#ifndef __FW_COMMON_H__
#define __FW_COMMON_H__

#define _ARDUINO_



#ifndef _ARDUINO_
#include <iostream>
#endif

#include <stdint.h>

#define Broadcast_id  0xFFFFFFFF //X = -1 , Y = -1
#define Server_id  0xFFFF0000    //X=0, Y = -1
#define NULL_ID 0xAAAABBBB

union unique_id_t
{
    struct {
        int16_t X;
        int16_t Y;
    }dimension;
    uint32_t ID;
};



typedef enum{
    move_it_up=0,
    move_it_down=1,
    move_it_left=2,
    move_it_right=3
}direction_t;



typedef enum{

    CTRL_CMD_I_AM_ALIVE_DUDE = ' ',        // should send every one minute to CPU

    CTRL_CMD_WAZZUP_BRO = 'a',            // PING Requested from cpu to module
    CTRL_CMD_WAZZUP_DUDE = 'A',           // PING answer from module to cpu

    CTRL_CMD_HI_FELLAS_I_AM_NEW_HELP_ME = 'n',  // the module need to his na
    CTRL_CMD_HERE_IS_YOUR_IP_BRO = 'i',
    CTRL_CMD_TNX_I_SET_MY_IP_DUDE = 'I',


    CTRL_CMD_TRANSACT_ME_SOME_BOX_BRO = 'b',       // command from cpu to module for moveing object
    CTRL_CMD_OMG_TRANSACTED_BOX_DUDE = 'B',       //  response to cpu transact done

    CTRL_CMD_FELLAS_OBJECT_DETECTED = 'D',       //command to cpu and neighbors object detected

} ctrl_cmd_t;



struct application_t{
  uint8_t proto_version;    //version of the protocol
  uint8_t ctrl_cmd;              //command which should exacuted
  uint16_t reserved;
  union{
    struct{
      uint8_t direction;
      uint32_t time_stamp;
    }move_object;
    unique_id_t ID_to_set;
  }info;
};




#endif
