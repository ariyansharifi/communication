//
// Created by Ariyan on 9/2/2019.
//

#ifndef SERVER_APPLICATION_LAYER_HPP
#define SERVER_APPLICATION_LAYER_HPP


#include "fw_common.hpp"
#include "network_layer.hpp"
namespace CPU{


    class application_layer {
    public:
        typedef std::shared_ptr<CPU::application_layer> sptr;
        static CPU::application_layer::sptr make();

        void send_move_the_box(CPU::direction_t _dir,unique_id_t _this_module);
        void init_0_0_module_ID();
        void handle_rx_pkt();
    private:
        application_layer();
        void send_pkt_struct(const CPU::application_t& pkt,CPU::unique_id_t _to_this_module, CPU::packet_policy_t _policy = CPU::packet_policy_t::deliver_command_via_broadcast) ;
        bool rcv_pkt_struct( application_t& pkt,CPU::unique_id_t _from_this_module);

        network_layer::sptr _network;
    };

}



#endif //SERVER_APPLICATION_LAYER_HPP
