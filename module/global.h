#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include "fw_common.h"
#include "communication_layer.h"

#include <SoftwareSerial.h>

extern unique_id_t my_id;
extern packet_t rxtx_packet;


extern SoftwareSerial Upport;
extern SoftwareSerial Downport;
extern SoftwareSerial Leftport;
extern SoftwareSerial Rightport;


#endif
