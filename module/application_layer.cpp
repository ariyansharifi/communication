#include "application_layer.h"
#include "network_layer.h"
#include "fw_common.h"

void handle_RX_ctrl_cmd()  {
    static unique_id_t _rx_packet_source_id;
    static uint8_t n;

    static application_t pkt_o;
    uint8_t* raw_data_o = reinterpret_cast<uint8_t*>(&pkt_o);

    static uint8_t raw_data_i [50];
    if(!network_layer::handle_RX_packet(raw_data_i,n,_rx_packet_source_id))
        return;
    application_t* pkt_i = reinterpret_cast<application_t*>(raw_data_i);
    switch (pkt_i->ctrl_cmd){

        ///////////////////////////////////////////////////////////////////////////
        /////                           MOVING BOX                              //
        //////////////////////////////////////////////////////////////////////////
        case ctrl_cmd_t::CTRL_CMD_TRANSACT_ME_SOME_BOX_BRO:             // command to move the box

        break;

        ///////////////////////////////////////////////////////////////////////////
        /////                           PING REQUEST FROM PC                    //
        //////////////////////////////////////////////////////////////////////////

        case ctrl_cmd_t::CTRL_CMD_WAZZUP_BRO:
            pkt_o.ctrl_cmd = ctrl_cmd_t::CTRL_CMD_WAZZUP_DUDE;
            network_layer::handle_TX_packet(raw_data_o,sizeof(application_t),Server_id,packet_policy_t::deliver_command_via_broadcast);         //answer I am alive
            break;
        ///////////////////////////////////////////////////////////////////////////
        /////                           CHANGE IP REQUEST                        //
        //////////////////////////////////////////////////////////////////////////


        case ctrl_cmd_t::CTRL_CMD_HERE_IS_YOUR_IP_BRO:
                my_id = pkt_i->info.ID_to_set;              //a packet to set ID
           break;


        ///////////////////////////////////////////////////////////////////////////
        /////                           ADD WHAT EVER YOU WANT                  //
        //////////////////////////////////////////////////////////////////////////



    }
}


void send_I_am_alive(){

      application_t pkt_o;
      uint8_t* raw_data_o = reinterpret_cast<uint8_t*>(&pkt_o);

    pkt_o.ctrl_cmd = ctrl_cmd_t::CTRL_CMD_I_AM_ALIVE_DUDE;
    network_layer::handle_TX_packet(raw_data_o,sizeof(pkt_o),Server_id,packet_policy_t::deliver_command_via_broadcast);
}
