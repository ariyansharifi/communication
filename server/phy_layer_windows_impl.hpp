//
// Created by Ariyan on 9/1/2019.
//

#ifndef SERVER_PHY_LAYER_WINDOWS_IMPL_HPP
#define SERVER_PHY_LAYER_WINDOWS_IMPL_HPP

#include "phy_layer.hpp"
#include <windows.h>
#include <string>

namespace CPU{

class phy_layer_windows_impl: public phy_layer {
public:
    bool read_one_byte(uint8_t& data_i);
    void write_one_byte(uint8_t data_o);
    phy_layer_windows_impl(std::string com_port, double Timeout);
    ~phy_layer_windows_impl();
private:
    HANDLE  hComm;
    DWORD dNoOFBytestoWrit=1;         // No of bytes to write into the port
    DWORD dNoOfBytesWritten = 0;     // No of bytes written to the port


};

}



#endif //SERVER_PHY_LAYER_WINDOWS_IMPL_HPP
