#include "global.h"

unique_id_t my_id;
packet_t rxtx_packet;   //using one struct for both RX ad TX that avoid some copy between functions

SoftwareSerial Upport(10, 11);      // software serial #1: RX = digital pin 10, TX = digital pin 11
SoftwareSerial Downport(12,13);     // software serial #2: RX = digital pin 12, TX = digital pin 13
SoftwareSerial Leftport(52, 53);    // software serial #3: RX = digital pin 52, TX = digital pin 53
SoftwareSerial Rightport(50, 51);   // software serial #4: RX = digital pin 50, TX = digital pin 51
